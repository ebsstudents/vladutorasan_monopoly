﻿using Monopoly.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly.Entities
{
    class Cell
    {
        string name;
        int cost;
        int rent;
        int position;
        Constants.PlayerTypes owner;
        int x, y;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
            }
        }

        public int X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
            }
        }

        public PlayerTypes Owner
        {
            get
            {
                return this.owner;
            }
            set
            {
                this.owner = value;
            }
        }

        public int Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public int Rent
        {
            get
            {
                return this.rent;
            }
            set
            {
                this.rent = value;
            }
        }

        public int Cost
        {
            get
            {
                return this.cost;
            }
            set
            {
                this.cost = value;
            }
        }
    }
}
