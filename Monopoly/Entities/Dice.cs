﻿using System;

namespace Monopoly.Entities
{
    class Dice
    {
        static Random dice = new Random();
        static public int Roll()
        {
            return dice.Next(1, 7);
        }
    }
}