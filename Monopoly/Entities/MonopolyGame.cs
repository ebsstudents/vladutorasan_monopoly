﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly.Entities
{
    class MonopolyGame
    {
        public List<Player> playersList = new List<Player>();
        public List<Cell> board = new List<Cell>();
        public Dice dice;
    }
}
