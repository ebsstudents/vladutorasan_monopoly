﻿using Monopoly.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly.Entities
{
    public class Player
    {
        #region private members
            private string name;
            private PlayerTypes type;
            private int money;
        #endregion

        #region public members
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public PlayerTypes Type
        {
            get { return type; }
            set { type = value; }
        }

        public int Money
        {
            get { return money; }
            set { money = value; }
        }
        #endregion
    }
}
