﻿namespace Monopoly.Constants
{
    public enum PlayerTypes
    {
        Car,
        Shoe,
        Plane,
        Boat,
        Tank
    }
}