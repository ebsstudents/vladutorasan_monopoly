﻿namespace Monopoly.Forms
{
    partial class MonopolyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MonopolyGameSplitContainer = new System.Windows.Forms.SplitContainer();
            this.PlayersDataGridView = new System.Windows.Forms.DataGridView();
            this.TurnLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.RollDiceButton = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.MonopolyGameSplitContainer)).BeginInit();
            this.MonopolyGameSplitContainer.Panel1.SuspendLayout();
            this.MonopolyGameSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayersDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // MonopolyGameSplitContainer
            // 
            this.MonopolyGameSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MonopolyGameSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MonopolyGameSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.MonopolyGameSplitContainer.Name = "MonopolyGameSplitContainer";
            // 
            // MonopolyGameSplitContainer.Panel1
            // 
            this.MonopolyGameSplitContainer.Panel1.Controls.Add(this.textBox2);
            this.MonopolyGameSplitContainer.Panel1.Controls.Add(this.RollDiceButton);
            this.MonopolyGameSplitContainer.Panel1.Controls.Add(this.textBox1);
            this.MonopolyGameSplitContainer.Panel1.Controls.Add(this.TurnLabel);
            this.MonopolyGameSplitContainer.Panel1.Controls.Add(this.PlayersDataGridView);
            this.MonopolyGameSplitContainer.Size = new System.Drawing.Size(736, 381);
            this.MonopolyGameSplitContainer.SplitterDistance = 278;
            this.MonopolyGameSplitContainer.TabIndex = 0;
            // 
            // PlayersDataGridView
            // 
            this.PlayersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PlayersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PlayersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PlayersDataGridView.Location = new System.Drawing.Point(3, -1);
            this.PlayersDataGridView.Name = "PlayersDataGridView";
            this.PlayersDataGridView.ReadOnly = true;
            this.PlayersDataGridView.Size = new System.Drawing.Size(272, 241);
            this.PlayersDataGridView.TabIndex = 0;
            // 
            // TurnLabel
            // 
            this.TurnLabel.AutoSize = true;
            this.TurnLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TurnLabel.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TurnLabel.Location = new System.Drawing.Point(11, 263);
            this.TurnLabel.Name = "TurnLabel";
            this.TurnLabel.Size = new System.Drawing.Size(66, 24);
            this.TurnLabel.TabIndex = 1;
            this.TurnLabel.Text = "Turn : ";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(83, 263);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 23);
            this.textBox1.TabIndex = 2;
            // 
            // RollDiceButton
            // 
            this.RollDiceButton.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RollDiceButton.Location = new System.Drawing.Point(55, 324);
            this.RollDiceButton.Name = "RollDiceButton";
            this.RollDiceButton.Size = new System.Drawing.Size(71, 25);
            this.RollDiceButton.TabIndex = 3;
            this.RollDiceButton.Text = "Roll dice ";
            this.RollDiceButton.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(132, 324);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(43, 25);
            this.textBox2.TabIndex = 4;
            // 
            // MonopolyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 381);
            this.Controls.Add(this.MonopolyGameSplitContainer);
            this.Name = "MonopolyForm";
            this.Text = "MonopolyGame";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MonopolyForm_FormClosed);
            this.Load += new System.EventHandler(this.MonopolyForm_Load);
            this.MonopolyGameSplitContainer.Panel1.ResumeLayout(false);
            this.MonopolyGameSplitContainer.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MonopolyGameSplitContainer)).EndInit();
            this.MonopolyGameSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlayersDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer MonopolyGameSplitContainer;
        private System.Windows.Forms.DataGridView PlayersDataGridView;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button RollDiceButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label TurnLabel;
    }
}