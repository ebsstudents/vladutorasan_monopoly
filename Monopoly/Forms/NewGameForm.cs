﻿using Monopoly.Constants;
using Monopoly.Entities;
using Monopoly.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly
{
    public partial class NewGameForm : Form
    {
        List<Player> playersList = new List<Player>();
        public NewGameForm()
        {
            InitializeComponent();
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            playersList.RemoveAll(player => player.Money == 0 || player.Name == string.Empty);
            MonopolyForm monopolyForm = new MonopolyForm(playersList);
            //monopolyForm.ShowDialog();
            monopolyForm.Show();
            this.Hide();
        }
        private void InitilazePlayerList()
        {
            playersList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Boat });
            playersList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Car });
            playersList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Plane });
            playersList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Shoe });
            playersList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Tank });
        }

        private void NewGameForm_Load(object sender, EventArgs e)
        {
            InitilazePlayerList();
            PlayersDataGridView.DataSource = playersList;
        }
    }
}
