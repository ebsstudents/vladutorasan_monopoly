﻿namespace Monopoly
{
    partial class NewGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NewGameSplitContainer = new System.Windows.Forms.SplitContainer();
            this.PlayersDataGridView = new System.Windows.Forms.DataGridView();
            this.QuitButton = new System.Windows.Forms.Button();
            this.NewGameButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NewGameSplitContainer)).BeginInit();
            this.NewGameSplitContainer.Panel1.SuspendLayout();
            this.NewGameSplitContainer.Panel2.SuspendLayout();
            this.NewGameSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayersDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // NewGameSplitContainer
            // 
            this.NewGameSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NewGameSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewGameSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.NewGameSplitContainer.Name = "NewGameSplitContainer";
            this.NewGameSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // NewGameSplitContainer.Panel1
            // 
            this.NewGameSplitContainer.Panel1.Controls.Add(this.PlayersDataGridView);
            // 
            // NewGameSplitContainer.Panel2
            // 
            this.NewGameSplitContainer.Panel2.Controls.Add(this.QuitButton);
            this.NewGameSplitContainer.Panel2.Controls.Add(this.NewGameButton);
            this.NewGameSplitContainer.Size = new System.Drawing.Size(557, 380);
            this.NewGameSplitContainer.SplitterDistance = 273;
            this.NewGameSplitContainer.TabIndex = 0;
            // 
            // PlayersDataGridView
            // 
            this.PlayersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PlayersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PlayersDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PlayersDataGridView.Location = new System.Drawing.Point(0, 0);
            this.PlayersDataGridView.Name = "PlayersDataGridView";
            this.PlayersDataGridView.RowHeadersVisible = false;
            this.PlayersDataGridView.Size = new System.Drawing.Size(555, 271);
            this.PlayersDataGridView.TabIndex = 0;
            // 
            // QuitButton
            // 
            this.QuitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.QuitButton.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuitButton.Location = new System.Drawing.Point(245, 67);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.Size = new System.Drawing.Size(75, 23);
            this.QuitButton.TabIndex = 1;
            this.QuitButton.Text = "QUIT";
            this.QuitButton.UseVisualStyleBackColor = true;
            // 
            // NewGameButton
            // 
            this.NewGameButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NewGameButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewGameButton.Location = new System.Drawing.Point(201, 17);
            this.NewGameButton.Name = "NewGameButton";
            this.NewGameButton.Size = new System.Drawing.Size(162, 33);
            this.NewGameButton.TabIndex = 0;
            this.NewGameButton.Text = "New Game";
            this.NewGameButton.UseVisualStyleBackColor = true;
            this.NewGameButton.Click += new System.EventHandler(this.NewGameButton_Click);
            // 
            // NewGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 380);
            this.Controls.Add(this.NewGameSplitContainer);
            this.Name = "NewGameForm";
            this.Text = "NEW GAME";
            this.Load += new System.EventHandler(this.NewGameForm_Load);
            this.NewGameSplitContainer.Panel1.ResumeLayout(false);
            this.NewGameSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NewGameSplitContainer)).EndInit();
            this.NewGameSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlayersDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer NewGameSplitContainer;
        private System.Windows.Forms.DataGridView PlayersDataGridView;
        private System.Windows.Forms.Button QuitButton;
        private System.Windows.Forms.Button NewGameButton;
    }
}

